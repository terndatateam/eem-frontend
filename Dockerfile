# stage 1
# Create a docker image which will build the app
# FROM node:12-alpine as node
# WORKDIR /app
# COPY . .
# RUN npm install
# RUN npm run build:staging
#RUN npm run build:staging-aekos

# stage 2
# Create an image which will deploy the application
#Note: A docker container based on this image will
#Always point to the staging API (see RUN npm run build:staging about)
FROM nginx:alpine
# COPY --from=node /app/dist /usr/share/nginx/html/
COPY ./dist /usr/share/nginx/html/
#COPY --from=node /app/default.conf /etc/nginx/conf.d/default.conf
