import { Component, Input, OnInit } from '@angular/core';

import { NbMenuService, NbSidebarService } from '@nebular/theme';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  version = 'ALPHA 1.0.4'; // TODO pull this from package.json

  @Input() position = 'normal';

  constructor(private sidebarService: NbSidebarService,
    private menuService: NbMenuService) {
  }

  ngOnInit() { }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }
}
