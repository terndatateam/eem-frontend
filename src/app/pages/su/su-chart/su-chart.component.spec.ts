// import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { ThemeModule } from '../../../@theme/theme.module';
import { NbThemeModule, NbThemeService } from '@nebular/theme';
import { ChartingService } from '../../services/charting.service';
import { SuService } from '../su.service';

// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';

// import { ChartModule, ChartComponent } from 'angular2-chartjs';
import { ChartModule } from 'angular2-chartjs';

import { SuChartComponent } from './su-chart.component';

import * as test_data from './../../../test-data/su.json';

// import { Observable } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';


xdescribe('SuChartComponent', () => {
  let component: SuChartComponent;
  let fixture: ComponentFixture<SuChartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule, ChartModule, ThemeModule, NbThemeModule ],
      providers: [ SuService, ChartingService, NbThemeService ],
      declarations: [ SuChartComponent ],
    });

    fixture = TestBed.createComponent(SuChartComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should populate cdata property with label data', () => {
  // const chartService = TestBed.get(ChartingService);
  // spyOn(chartService, '')
  const suService = TestBed.get(SuService);
  const theme = TestBed.get(NbThemeService);
  // const suChartService = TestBed.get(ChartingService);
  // spyOn(suService, 'getSamplingUnits').and.returnValue(Observable.from([ [{id: 1}, {id: 2}] ]));
  spyOn(suService, 'getSamplingUnits').and.returnValue(Observable.from([ [test_data] ]));

  spyOn(theme, 'getJsTheme').and.returnValue(Observable.from([ [{id: 1}] ]));


  // fixture.detectChanges();

  // Assert - expect()
  // console.log('cdata=' + JSON.stringify(component.cdata));

  });
});
