import { Component, OnInit } from '@angular/core';
import { NbThemeService, NbColorHelper } from '@nebular/theme';
import { SuService } from '../su.service'
import { ChartingService } from '../../services/charting.service';

@Component({
  templateUrl: './su-chart.component.html',
  styleUrls: ['./su-chart.component.scss'],
})
export class SuChartComponent implements OnInit {
  cdata: any;
  coptions: any;
  themeSubscription: any;
  rawData: any[];

  constructor(private samplintUnitService: SuService, private theme: NbThemeService, private chartService: ChartingService) {
  }


  ngOnInit() {
    this.samplintUnitService.getSamplingUnits().subscribe(data => {
      const cleaned = (data as Array<any>).reduce((accum, curr) => {
        if (curr.ultimateFeatureOfInterest) {
          curr.ultimateFeatureOfInterest = curr.ultimateFeatureOfInterest.replace(/[\w\.]*\/[\w\.]*\/[\w\.]*\//, '');
        }
        accum.push(this.replaceNulls(curr));
        return accum;
      }, [])
      this.rawData = cleaned;

      this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
        const colors: any = config.variables;
        const chartjs: any = config.variables.chartjs;
        const chartData = this.chartService.computeChartData(this.getArea());
        this.cdata = {
          labels: chartData.labels,
          datasets: [{
            data: chartData.data,
            label: 'Area',
            backgroundColor: NbColorHelper.hexToRgbA(colors.primaryLight, 0.8),
          }],
        };

        this.coptions = {
          maintainAspectRatio: false,
          responsive: true,
          legend: {
            labels: {
              fontColor: chartjs.textColor,
            },
          },
          scales: {
            xAxes: [
              {
                gridLines: {
                  display: false,
                  color: chartjs.axisLineColor,
                },
                ticks: {
                  fontColor: chartjs.textColor,
                },
              },
            ],
            yAxes: [
              {
                gridLines: {
                  display: true,
                  color: chartjs.axisLineColor,
                },
                ticks: {
                  fontColor: chartjs.textColor,
                  stepSize: 1,
                  beginAtZero: true,
                },
              },
            ],
          },
        };
      });
    })
  }


  replaceNulls(record) {
    const noValuePlaceHolder = '-';
    for (const key of Object.keys(record)) {
      const value = record[key];
      record[key] = value || noValuePlaceHolder;
    }
    return record;
  }

  getArea() {
    if (!this.rawData) {
      return [];
    }
    return this.rawData.reduce((accum, curr) => {
      if (curr.property !== 'Area') {
        return accum;
      }
      accum.push(curr.value);
      return accum;
    }, [])
  }

  }

  // constructor(private samplintUnitService: SuService, private theme: NbThemeService, private chartService: ChartingService) {

  //   this.samplintUnitService.getSamplingUnits().subscribe(data => {
  //     const cleaned = (data as Array<any>).reduce((accum, curr) => {
  //       if (curr.ultimateFeatureOfInterest) {
  //         curr.ultimateFeatureOfInterest = curr.ultimateFeatureOfInterest.replace(/[\w\.]*\/[\w\.]*\/[\w\.]*\//, '');
  //       }
  //       accum.push(this.replaceNulls(curr));
  //       return accum;
  //     }, [])
  //     this.rawData = cleaned;

  //     this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
  //       const colors: any = config.variables;
  //       const chartjs: any = config.variables.chartjs;
  //       const chartData = this.chartService.computeChartData(this.getArea());
  //       this.cdata = {
  //         labels: chartData.labels,
  //         datasets: [{
  //           data: chartData.data,
  //           label: 'Area',
  //           backgroundColor: NbColorHelper.hexToRgbA(colors.primaryLight, 0.8),
  //         }],
  //       };

  //       this.coptions = {
  //         maintainAspectRatio: false,
  //         responsive: true,
  //         legend: {
  //           labels: {
  //             fontColor: chartjs.textColor,
  //           },
  //         },
  //         scales: {
  //           xAxes: [
  //             {
  //               gridLines: {
  //                 display: false,
  //                 color: chartjs.axisLineColor,
  //               },
  //               ticks: {
  //                 fontColor: chartjs.textColor,
  //               },
  //             },
  //           ],
  //           yAxes: [
  //             {
  //               gridLines: {
  //                 display: true,
  //                 color: chartjs.axisLineColor,
  //               },
  //               ticks: {
  //                 fontColor: chartjs.textColor,
  //                 stepSize: 1,
  //                 beginAtZero: true,
  //               },
  //             },
  //           ],
  //         },
  //       };
  //     });
  //   })
  // }


  // replaceNulls(record) {
  //   const noValuePlaceHolder = '-';
  //   for (const key of Object.keys(record)) {
  //     const value = record[key];
  //     record[key] = value || noValuePlaceHolder;
  //   }
  //   return record;
  // }

  // getArea() {
  //   if (!this.rawData) {
  //     return [];
  //   }
  //   return this.rawData.reduce((accum, curr) => {
  //     if (curr.property !== 'Area') {
  //       return accum;
  //     }
  //     accum.push(curr.value);
  //     return accum;
  //   }, [])
  // }

// }

