import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { GridApi } from 'ag-grid'
import { SuService } from '../su.service'
import { buildGetRowsFn } from '../../eem-utils/query-utils'

@Component({
  templateUrl: './su-table.component.html',
  styleUrls: ['./su-table.component.scss'],
})
export class SuTableComponent implements OnInit {

  // FIXME make scroll bars fatter
  // FIXME allow copy+paste from cells - enterprise only?

  columnDefs = [
    {
      headerName: 'State',
      field: 'stateProvince',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Dataset Name',
      field: 'datasetName',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Survey ID',
      field: 'surveyId',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Sampling Unit ID',
      field: 'samplingUnitId',
      valueFormatter: params => {
        let result = params.value
        if (result) {
          result = result.replace(/.*\/(.*)>/, '$1')
        }
        return result
      },
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Custodian', field: 'custodian',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Rights',
      field: 'rights',
      cellRenderer: params => params.value,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Bibliographic Reference',
      field: 'bibliographicReference',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Date Modified',
      field: 'dateModified',
      filter: 'agDateColumnFilter',
      valueGetter: params => {
        if (params['data']) {
          return timeStrToDate(params.data.dateModified)
        }
      },
      valueFormatter: params => formatDate(params.value),
    },
    {
      headerName: 'Language', field: 'language',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Survey Name',
      field: 'surveyName',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Survey Organisation',
      field: 'surveyOrganisation',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Survey Type',
      field: 'surveyType',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Survey Methodology',
      field: 'surveyMethodology',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Survey Methodology Description',
      field: 'surveyMethodologyDescription',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Site ID',
      field: 'siteId',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Original Site Code',
      field: 'originalSiteCode',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Geodetic Datum',
      field: 'geodeticDatum',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Latitude (decimal)',
      field: 'decimalLatitude',
      type: 'numericColumn',
      filter: 'agNumberColumnFilter',
    },
    {
      headerName: 'Longitude (decimal)',
      field: 'decimalLongitude',
      type: 'numericColumn',
      filter: 'agNumberColumnFilter',
    },
    {
      headerName: 'Location Description',
      field: 'locationDescription',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Aspect',
      field: 'aspect',
      type: 'numericColumn',
      valueFormatter: coalesceFormatter,
      filter: 'agNumberColumnFilter',
    },
    {
      headerName: 'Slope',
      field: 'slope',
      type: 'numericColumn',
      valueFormatter: coalesceFormatter,
      filter: 'agNumberColumnFilter',
    },
    {
      headerName: 'Landform Pattern',
      field: 'landformPattern',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Landform Element',
      field: 'landformElement',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Elevation',
      field: 'elevation',
      valueFormatter: coalesceFormatter,
      filter: 'agNumberColumnFilter',
    },
    {
      headerName: 'Visit ID',
      field: 'visitId',
      valueFormatter: coalesceFormatter,
    },
    {
      headerName: 'Visit Date',
      field: 'visitDate',
      filter: 'agDateColumnFilter',
      valueGetter: params => {
        if (params['data']) {
          return timeStrToDate(params.data.visitDate)
        }
      },
      valueFormatter: params => formatDate(params.value),
    },
    {
      headerName: 'Visit Organisation',
      field: 'visitOrganisation',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Visit Observers',
      field: 'visitObservers',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Site Description',
      field: 'siteDescription',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Condition',
      field: 'condition',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Structural Form',
      field: 'structuralForm',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Owner\'s Classification',
      field: 'ownerClassification',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Current Classification',
      field: 'currentClassification',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Sampling Unit Area',
      field: 'samplingUnitArea',
      type: 'numericColumn',
      valueFormatter: coalesceFormatter,
      filter: 'agNumberColumnFilter',
    },
    {
      headerName: 'Sampling Unit Shape',
      field: 'samplingUnitShape',
      valueFormatter: coalesceFormatter,
      filter: 'agTextColumnFilter',
    },
  ]

  gridApi: GridApi
  gridColumnApi: any
  presetSurveyIdFilter: string

  rowCount: null;
  paginationPageSize = 100;
  cacheBlockSize = 100;
  enableServerSideSorting = true;
  enableServerSideFilter = true;
  infiniteInitialRowCount = 1;
  rowModelType = 'infinite'

  constructor(private service: SuService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.presetSurveyIdFilter = this.route.snapshot.queryParamMap.get('surveyId')
  }

  filterChanged() {
    const isNoPreset = !this.presetSurveyIdFilter
    if (isNoPreset) {
      return
    }
    const filterModel = this.gridApi.getFilterInstance('surveyId').getModel()
    const isInitialSetOnPageLoad = filterModel && this.presetSurveyIdFilter.toLowerCase() === filterModel.filter.toLowerCase()
    if (isInitialSetOnPageLoad) {
      return
    }
    this.presetSurveyIdFilter = null
  }


  onGridReady(params) {
    this.gridApi = params.api
    this.gridColumnApi = params.columnApi;

    if (this.presetSurveyIdFilter) {
      this.gridApi.getFilterInstance('surveyId').setModel({
        filter: this.presetSurveyIdFilter,
        type: 'equals',
      })
    }
    const datasource = {
      rowCount: this.rowCount,
      getRows: buildGetRowsFn((qp) => this.service.getSamplingUnitsServerPagination(qp)),
    }
    this.gridApi.setDatasource(datasource)
  }


  export() {
    const onlyVisibleColumns = false
    const params = {
      skipHeader: false,
      allColumns: onlyVisibleColumns,
      fileName: 'eem-samplingunits.csv',
      columnSeparator: ',',
    }
    this.gridApi.exportDataAsCsv(params);
  }

}

function coalesceFormatter(params) {
  return params.value === null || typeof (params.value) === 'undefined' ? '-' : params.value
}

function timeStrToDate(value) {
  if (!value) {
    return null
  }
  // FIXME we really need to store dates in RDF as numbers, not strings
  const date = new Date(value)
  return date
}

function formatDate(dateObj) {
  if (!dateObj) {
    return '-'
  }
  return dateObj.toLocaleDateString()
}



