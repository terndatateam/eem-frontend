import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { SuService } from './su.service';
import { environment } from '../../../environments/environment';

describe('SuService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SuService],
    });
  });

  afterEach((inject([HttpTestingController], (httpMock: HttpTestingController) => {
    // TODO does this work when we inject?
    httpMock.verify();
  })));

  it('should be created', inject([SuService], (service: SuService) => {
    expect(service).toBeTruthy();
  }));

  it('should return all study areas', inject([SuService, HttpTestingController],
      (service: SuService, httpMock: HttpTestingController) => {
    service.getSamplingUnits().subscribe((result: number[]) => {
      expect(result.length).toBe(2);
    })
    const req = httpMock.expectOne(`${environment.apiUrl}/eemsitedata`);
    expect(req.request.method).toBe('GET');
    req.flush([1, 2]); // TODO make better mock data
  }));
});
