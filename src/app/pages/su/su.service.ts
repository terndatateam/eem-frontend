import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class SuService {

  constructor(private http: HttpClient) { }

  getSamplingUnits() {
    const url = environment.apiUrl + '/eemsitedata'
    return this.http.get(url)
  }

  getSamplingUnitsServerPagination(qryParams) {
    const url = environment.apiUrl + '/eemsitedata'
    return this.http.get<any[]>(url, {
      params: qryParams,
    })
  }
}
