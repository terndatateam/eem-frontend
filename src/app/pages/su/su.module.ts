import { ChartingService } from '../services/charting.service'
import { NgModule } from '@angular/core'
import { Ng2SmartTableModule } from 'ng2-smart-table'
import { ChartModule } from 'angular2-chartjs'
import { AgGridModule } from 'ag-grid-angular'

import { ThemeModule } from '../../@theme/theme.module'
import { SuService } from './su.service'
import { SuChartComponent } from './su-chart/su-chart.component'
import { SuTableComponent } from './su-table/su-table.component'

@NgModule({
  imports: [
    ThemeModule,
    Ng2SmartTableModule,
    ChartModule,
    AgGridModule.withComponents([]),
  ],
  declarations: [
    SuChartComponent,
    SuTableComponent,
  ],
  providers: [
    SuService, ChartingService,
  ],
})
export class SuModule { }
