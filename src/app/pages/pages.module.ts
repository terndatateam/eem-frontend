import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { OrganismModule } from './organism/organism.module';
import { SuModule } from './su/su.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { EemApiModule } from './eem-api/eem-api.module';
import { EemDashboardUsageModule } from './eem-dashboard-usage/eem-dashboard-usage.module';
import { EemDownloadModule } from './eem-download/eem-download.module';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    OrganismModule,
    SuModule,
    EemApiModule,
    EemDashboardUsageModule,
    EemDownloadModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
})
export class PagesModule {
}
