import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Main',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'How to Use',
    icon: 'nb-help',
    link: '/pages/eem-dashboard-usage',
  },
  {
    title: 'Observations',
    icon: 'nb-tables',
    children: [
      {
        title: 'Data Table',
        link: '/pages/organism-table',
      },
    ],
  }, {
    title: 'Site Data',
    icon: 'nb-location',
    children: [
      {
        title: 'Data Table',
        link: '/pages/su',
      },
    ],
  },
  {
    title: 'EEM API',
    icon: 'nb-gear',
    link: '/pages/eem-api-doc',
  },
  {
    title: 'Data Download',
    icon: 'nb-drop',
    link: '/pages/eem-download',
  },
];
