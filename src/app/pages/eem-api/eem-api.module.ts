import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EemApiDocComponent } from './eem-api-doc/eem-api-doc.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [EemApiDocComponent],
})
export class EemApiModule { }
