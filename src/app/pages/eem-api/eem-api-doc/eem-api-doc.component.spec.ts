import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EemApiDocComponent } from './eem-api-doc.component';

describe('EemApiDocComponent', () => {
  let component: EemApiDocComponent;
  let fixture: ComponentFixture<EemApiDocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EemApiDocComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EemApiDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
