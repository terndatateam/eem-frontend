import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment'

@Component({
  selector: 'ngx-eem-api-doc',
  templateUrl: './eem-api-doc.component.html',
  styleUrls: ['./eem-api-doc.component.scss'],
})
export class EemApiDocComponent implements OnInit {

  eemAPIDocUrl = environment.apiUrl + '/';
  observationsEndPointUrl = environment.apiUrl + '/eemobservations';
  siteDataEndPointUrl = environment.apiUrl + '/eemsitedata';

  constructor() { }

  ngOnInit() {
  }

}
