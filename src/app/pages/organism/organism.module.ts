import { NgModule } from '@angular/core'
import { Ng2SmartTableModule } from 'ng2-smart-table'
import { ChartModule } from 'angular2-chartjs'
import { AgGridModule } from 'ag-grid-angular'
import { RouterModule } from '@angular/router'

import { ThemeModule } from '../../@theme/theme.module'
import { OrganismTableComponent } from './organism-table/organism-table.component'
import { OrganismChartComponent } from './organism-chart/organism-chart.component'
import { OrganismService } from './organism.service'
import { LinkableIdComponent } from './linkable-id/linkable-id.component'


@NgModule({
  imports: [
    ThemeModule,
    Ng2SmartTableModule,
    ChartModule,
    RouterModule,
    AgGridModule.withComponents([
      LinkableIdComponent,
    ]),
  ],
  declarations: [
    OrganismTableComponent,
    OrganismChartComponent,
    LinkableIdComponent,
  ],
  providers: [
    OrganismService,
  ],
})
export class OrganismModule { }
