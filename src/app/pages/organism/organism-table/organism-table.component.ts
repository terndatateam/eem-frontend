import { Component, OnInit } from '@angular/core'

import { OrganismService } from '../organism.service'
import { LinkableIdComponent } from '../linkable-id/linkable-id.component'
import { buildGetRowsFn } from '../../eem-utils/query-utils'

@Component({
  templateUrl: './organism-table.component.html',
  styleUrls: ['./organism-table.component.scss'],
})
export class OrganismTableComponent implements OnInit {

  // FIXME make scroll bars fatter
  // FIXME allow copy+paste from cells - enterprise only?

  columnDefs = [
    {
      headerName: 'State',
      field: 'stateProvince',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Dataset Name',
      field: 'datasetName',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Survey ID',
      field: 'surveyId',
      cellRendererFramework: LinkableIdComponent,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Study Location ID',
      field: 'siteId',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Feature ID',
      field: 'featureId',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Sampling Unit ID',
      field: 'samplingUnitId',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Visit ID',
      field: 'visitId',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'FOI',
      field: 'featureOfInterest',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Ultimate FOI',
      field: 'ultimateFeatureOfInterest', // FIXME not in API
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'FOI Qualifier',
      field: 'featureOfInterestQualifier',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Original FOI', // FIXME not in API
      field: 'originalFeatureOfInterest',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Protocol',
      field: 'protocol',
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Property',
      field: 'characteristic',
      valueFormatter: coalesceFormatter,
      cellClass: muteZeroLengthCellStyler,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Property Qualifier',
      field: 'characteristicQualifier',
      valueFormatter: params => params.value === '' ? '(no qualifier)' : params.value, cellClass: muteZeroLengthCellStyler,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Value',
      field: 'value',
      valueFormatter: coalesceFormatter,
      cellClass: muteNaCellStyler,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Range Low', // FIXME not in API
      field: 'lower',
      valueFormatter: coalesceFormatter,
      cellClass: muteNaCellStyler,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Range High', // FIXME not in API
      field: 'upper',
      valueFormatter: coalesceFormatter,
      cellClass: muteNaCellStyler,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Category',
      field: 'category',
      valueFormatter: coalesceFormatter,
      cellClass: muteNaCellStyler,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Description', // FIXME not in API
      field: 'description',
      valueFormatter: coalesceFormatter,
      cellClass: muteNaCellStyler,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Standard',
      field: 'standard',
      valueFormatter: coalesceFormatter,
      cellClass: muteNaCellStyler,
      filter: 'agTextColumnFilter',
    },
  ]

  gridApi: any;
  columnApi: any;

  rowCount: null;
  paginationPageSize = 1000;
  cacheBlockSize = 1000;
  enableServerSideSorting = true;
  enableServerSideFilter = true;
  infiniteInitialRowCount = 1;
  rowModelType = 'infinite'

  constructor(private service: OrganismService) { }

  ngOnInit() { }

  onGridReady(params) {
    this.gridApi = params.api
    this.columnApi = params.columnApi
    const datasource = {
      rowCount: this.rowCount,
      getRows: buildGetRowsFn((qp) => this.service.getOrganisms(qp)),
    }
    this.gridApi.setDatasource(datasource)
  }


  export() {
    const onlyVisibleColumns = false
    const params = {
      skipHeader: false,
      allColumns: onlyVisibleColumns,
      fileName: 'eem-organisms.csv',
      columnSeparator: ',',
    }
    this.gridApi.exportDataAsCsv(params);
  }

}

function coalesceFormatter(params) {
  const isNull = params.value === null
  const isUndefined = typeof (params.value) === 'undefined'
  const isZeroLength = !params.value || params.value.length === 0
  if (isNull || isUndefined || isZeroLength) {
    return '-'
  }
  return params.value
}

function muteNaCellStyler(params) {
  return params.value === 'N/A' ? 'text-muted' : null
}

function muteZeroLengthCellStyler(params) {
  return !params.value || params.value.length === 0 ? 'text-muted' : null
}

