import { Component } from '@angular/core'

import { ICellRendererAngularComp } from 'ag-grid-angular'

import { fullSamplingUnitTable } from '../../path-fragments'

@Component({
  templateUrl: './linkable-id.component.html',
})
export class LinkableIdComponent implements ICellRendererAngularComp {

  url = fullSamplingUnitTable
  params: any

  agInit(params: any) {
    this.params = params
  }

  refresh(): boolean {
    return true
  }
}
