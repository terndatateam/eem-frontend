import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { OrganismService } from './organism.service';
import { environment } from '../../../environments/environment';

xdescribe('OrganismService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [OrganismService],
    });
  });

  afterEach((inject([HttpTestingController], (httpMock: HttpTestingController) => {
    // TODO does this work when we inject?
    httpMock.verify();
  })));

  it('should be created', inject([OrganismService], (service: OrganismService) => {
    expect(service).toBeTruthy();
  }));

  it('should return all study areas', inject([OrganismService, HttpTestingController],
      (service: OrganismService, httpMock: HttpTestingController) => {
    service.getOrganismsAll().subscribe((result: number[]) => {
      expect(result.length).toBe(2);
    })
    const req = httpMock.expectOne(`${environment.apiUrl}/organism`);
    expect(req.request.method).toBe('GET');
    req.flush([1, 2]); // TODO make better mock data
  }));

  it('should compute chart data', inject([OrganismService],
    (service: OrganismService) => {
    const rawHeights = ['1.5', '0.3', '1.5', '1', '0.3', '2', '0.3', '0.8', '0.3',
      '1.5', '1.5', '2', '1.2', '0.5', '2', '1', '0.2', '0.2', '0.6', '2', '-',
      '0.35', '0.1', '0.01', '2', '0.3', '0.2', '0.01', '1', '0.6', '0.3', '0.2',
      '7', '0.1', '0.6', '4', '20', '0.15', '0.15', '6.5'];
    const result = service.computeChartData(rawHeights);
    expect(result.labels.length).toBe(18);
    expect(result.data.length).toBe(18);
    expect(result.labels[0]).toBe('-');
    expect(result.labels[1]).toBe('0.01');
    expect(result.data[1]).toBe(2);
  }));
});
