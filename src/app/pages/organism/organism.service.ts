import { Injectable } from '@angular/core'
import { environment } from '../../../environments/environment'
import { HttpClient } from '@angular/common/http'
import { map } from 'rxjs/operators';

@Injectable()
export class OrganismService {

  constructor(
    private http: HttpClient,
  ) { }

  getOrganisms(qryParams) {
    const url = environment.apiUrl + `/eemobservations`;
    return this.http.get<any[]>(url, {
      params: qryParams,
    });
  }

  // TODO - remove this method
  getOrganismsAll() {
    const url = environment.apiUrl + '/eemobservations?limit=1000'
    return this.http.get(url)
  }

  getNumberOfOrganismRecords() {
    const url = environment.apiUrl + '/eemobservations?limit=1'
    const result = this.http.get(url, {
      observe: 'response',
      headers: {
        prefer: 'count=exact', // http://postgrest.org/en/v5.1/api.html#limits-and-pagination
      },
    })
    return result.pipe(
      map((resp) => {
        return resp.headers.get('Content-Range').replace(/.*\//, '')
      }),
    )
  }

  computeChartData(raw: string[]) {

    const store = raw.reduce((accum, curr) => {
      if (!accum[curr]) {
        accum[curr] = 0
      }
      accum[curr] += 1
      return accum
    }, {})

    const result = Object.keys(store).sort().reduce((accum, curr) => {
      accum.labels.push(curr)
      accum.data.push(store[curr])
      return accum
    }, {labels: [], data: []})
    return result
  }

}
