import { Component } from '@angular/core'
import { NbThemeService, NbColorHelper } from '@nebular/theme'

import { OrganismService } from '../organism.service'

@Component({
  templateUrl: './organism-chart.component.html',
})
export class OrganismChartComponent {
  cdata: any
  coptions: any
  themeSubscription: any
  rawData: any[]

  constructor(private service: OrganismService, private theme: NbThemeService) {
    this.service.getOrganismsAll().subscribe(data => {
      const cleaned = (data as Array<any>).reduce((accum, curr) => {
        if (curr.ultimateFeatureOfInterest) {
          curr.ultimateFeatureOfInterest = curr.ultimateFeatureOfInterest.replace(/[\w\.]*\/[\w\.]*\/[\w\.]*\//, '') // strip leading noise
        }
        accum.push(replaceNulls(curr))
        return accum
      }, [])
      this.rawData = cleaned

      this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

        const colors: any = config.variables
        const chartjs: any = config.variables.chartjs
        const chartData = this.service.computeChartData(this.getHeights())
        this.cdata = {
          labels: chartData.labels,
          datasets: [{
            data: chartData.data,
            label: 'Height',
            backgroundColor: NbColorHelper.hexToRgbA(colors.primaryLight, 0.8),
          }],
        }

        this.coptions = {
          maintainAspectRatio: false,
          responsive: true,
          legend: {
            labels: {
              fontColor: chartjs.textColor,
            },
          },
          scales: {
            xAxes: [
              {
                gridLines: {
                  display: false,
                  color: chartjs.axisLineColor,
                },
                ticks: {
                  fontColor: chartjs.textColor,
                },
              },
            ],
            yAxes: [
              {
                gridLines: {
                  display: true,
                  color: chartjs.axisLineColor,
                },
                ticks: {
                  fontColor: chartjs.textColor,
                  stepSize: 1,
                  beginAtZero: true,
                },
              },
            ],
          },
        }
      })
    })
  }

  getHeights() {
    if (!this.rawData) {
      return []
    }
    return this.rawData.reduce((accum, curr) => {
      if (curr.property !== 'Height') {
        return accum
      }
      accum.push(curr.value)
      return accum
    }, [])
  }
}

function replaceNulls (record) {
  const noValuePlaceholder = '-'
  for (const key of Object.keys(record)) {
    const value = record[key]
    record[key] = value || noValuePlaceholder // FIXME probably breaks down with 0 or false
  }
  return record
}
