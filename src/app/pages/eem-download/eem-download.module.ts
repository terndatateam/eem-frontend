import { NgModule } from '@angular/core';


import { ThemeModule } from '../../@theme/theme.module';
import { EemDownloadComponent } from './eem-download.component';
import { DatasetService } from '../services/dataset.service';

@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    EemDownloadComponent,
  ],
  providers: [
    DatasetService,
  ],
})
export class EemDownloadModule { }
