import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment'
import { DatasetService } from '../services/dataset.service';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './eem-download.component.html',
})
export class EemDownloadComponent implements OnInit {
  baseUrl = environment.apiUrl;
  datasets: any[];
  constructor(private service: DatasetService) { }

  ngOnInit() {
    this.service.getDatasets().subscribe((data: any[]) => {
      this.datasets = data;
    });
  }

}
