import { Injectable } from '@angular/core';

@Injectable()
export class ChartingService {

  constructor() { }

  computeChartData(raw: string[]) {
    const store = raw.reduce((accum, curr) => {
      if (!accum[curr]) {
        accum[curr] = 0
      }
      accum[curr] += 1
      return accum
    }, {})

    const result = Object.keys(store).sort().reduce((accum, curr) => {
      accum.labels.push(curr)
      accum.data.push(store[curr])
      return accum
    }, {labels: [], data: []})
    return result
  }
}
