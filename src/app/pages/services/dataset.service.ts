import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DatasetService {

  constructor(
    private http: HttpClient,
  ) { }

  getDatasets() {
    const url = environment.apiUrl + '/datasets';
    return this.http.get(url);
  }
}
