import { NgModule } from '@angular/core';


import { ThemeModule } from '../../@theme/theme.module';
import { EemDashboardUsageComponent } from './eem-dashboard-usage.component';


@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    EemDashboardUsageComponent,
  ],
})
export class EemDashboardUsageModule { }
