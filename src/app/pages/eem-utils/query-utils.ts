export function buildGetRowsFn (dataGetterFn) {
  return function (rowParams) {
    const start = rowParams.startRow
    const limit = rowParams.endRow - start
    const sortClause = rowParams.sortModel.reduce((accum, curr) => {
      accum.push(`${curr.colId}.${curr.sort.toLowerCase()}`)
      return accum
    }, [])
    const whereClause = buildWhereClause(rowParams.filterModel)
    const queryParams = Object.assign({
        limit: limit,
        offset: start,
      }, whereClause,
    )
    if (sortClause.length) {
      queryParams['order'] = sortClause.join(',')
    }
    dataGetterFn(queryParams).subscribe(data => {
      const dataSize = data.length
      let lastRow = -1
      if (dataSize < limit) {
        lastRow = start + dataSize
      }
      rowParams.successCallback(data, lastRow)
    })
  }
}

/**
 * A method which build a where clause compatible with
 * query parameters expected by PostgREST API.
 * It receives a filterModel object from ag-grid
 * @param filterModel
 */
function buildWhereClause(filterModel) {

    const result = Object.keys(filterModel).reduce((accum, currKey) => {
      const currAgFilter = filterModel[currKey]
      const value = currAgFilter.filter
      const agGridToPostgrestBasicMapping = {
        equals: 'eq',
        notEqual: 'neq',
        lessThan: 'lt',
        lessThanOrEqual: 'lte',
        greaterThan: 'gt',
        greaterThanOrEqual: 'gte',
      }
      const agGridCriteria = currAgFilter.type
      const simplePostgrestCriteria = agGridToPostgrestBasicMapping[agGridCriteria]
      if (simplePostgrestCriteria) {
        accum[currKey] = `${simplePostgrestCriteria}.${value}`
        return accum
      }
      const agGridToPostgrestComplexMapping = {
        startsWith: () => {
          accum[currKey] = `ilike.${value}*`
        },
        endsWith:  () => {
          accum[currKey] = `ilike.*${value}`
        },
        contains: () => {
          accum[currKey] = `ilike.*${value}*`
        },
        notContains: () => {
          accum[currKey] = `not.ilike.*${value}*`
        },
        inRange: () => {
          const filterTo = currAgFilter.filterTo
          accum['and'] = `${currKey}.gte.${value},${currKey}.lte.${filterTo}`
        },
      }
      const complexHandler = agGridToPostgrestComplexMapping[agGridCriteria]
      if (!complexHandler) {
        throw new Error(`Programmer problem: unsupported criteria encountered '${agGridCriteria}'`)
      }
      complexHandler()
      return accum
    }, {})
    return result
  }
