import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core'

import { PagesComponent } from './pages.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { OrganismTableComponent, OrganismChartComponent } from './organism'
import { SuChartComponent } from './su/su-chart/su-chart.component'
import { SuTableComponent } from './su'
import { samplingUnitTableRoute } from './path-fragments'
import { EemApiDocComponent } from './eem-api/eem-api-doc/eem-api-doc.component';
import { EemDashboardUsageComponent } from './eem-dashboard-usage/eem-dashboard-usage.component';
import { EemDownloadComponent } from './eem-download/eem-download.component';


const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
    }, {
      path: 'organism-table',
      component: OrganismTableComponent,
    }, {
      path: 'organism-chart',
      component: OrganismChartComponent,
    }, {
      path: samplingUnitTableRoute,
      component: SuTableComponent,
    }, {
      path: 'su-chart',
      component: SuChartComponent,
    }, {
      path: 'eem-api-doc',
      component: EemApiDocComponent,
    }, {
      path: 'eem-dashboard-usage',
      component: EemDashboardUsageComponent,
    }, {
      path: 'eem-download',
      component: EemDownloadComponent,
    }, {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
  ],
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
