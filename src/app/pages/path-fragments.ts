export const pages = 'pages'

export const samplingUnitTableRoute = 'su'
export const fullSamplingUnitTable = `/${pages}/${samplingUnitTableRoute}`
