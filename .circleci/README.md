## CircleCI config
You need to make sure the following [CircleCI env vars](https://circleci.com/docs/2.0/env-vars/) are configured:

  1. AWS credentials (`AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`)
  1. `PROD_BUCKET` env var for the production bucket name, e.g: `eem.example.com`
  1. `STAGING_BUCKET` env var for the staging bucket name, e.g: `staging.eem.example.com`

Set up the S3 buckets for deployment with the following `awscli` commands (replace `eem.example.com` with your bucket name):

  1. create the bucket

          aws s3api create-bucket \
            --bucket eem.example.com \
            --region ap-southeast-2 \
            --create-bucket-configuration LocationConstraint=ap-southeast-2

  1. configure the bucket for website hosting

          aws s3api put-bucket-website \
            --bucket eem.example.com \
            --website-configuration '{"IndexDocument":{"Suffix":"index.html"}}'


If you want to create an AWS user specifically for the CircleCI job, give it the following policy:
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:ListBucketByTags",
                "s3:GetLifecycleConfiguration",
                "s3:GetBucketTagging",
                "s3:GetInventoryConfiguration",
                "s3:GetObjectVersionTagging",
                "s3:ListBucketVersions",
                "s3:GetBucketLogging",
                "s3:ListBucket",
                "s3:GetAccelerateConfiguration",
                "s3:GetBucketPolicy",
                "s3:GetObjectVersionTorrent",
                "s3:GetObjectAcl",
                "s3:GetBucketRequestPayment",
                "s3:GetObjectVersionAcl",
                "s3:GetObjectTagging",
                "s3:GetMetricsConfiguration",
                "s3:PutObjectTagging",
                "s3:DeleteObject",
                "s3:GetIpConfiguration",
                "s3:ListBucketMultipartUploads",
                "s3:GetBucketWebsite",
                "s3:GetBucketVersioning",
                "s3:GetBucketAcl",
                "s3:GetBucketNotification",
                "s3:GetReplicationConfiguration",
                "s3:ListMultipartUploadParts",
                "s3:PutObject",
                "s3:PutObjectAcl",
                "s3:GetObject",
                "s3:GetObjectTorrent",
                "s3:GetBucketCORS",
                "s3:GetAnalyticsConfiguration",
                "s3:GetObjectVersionForReplication",
                "s3:GetBucketLocation",
                "s3:GetObjectVersion"
            ],
            "Resource": [
                "arn:aws:s3:::staging.eem.example.com",
                "arn:aws:s3:::staging.eem.example.com/*",
                "arn:aws:s3:::eem.example.com",
                "arn:aws:s3:::eem.example.com/*"
            ]
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "s3:HeadBucket",
                "s3:ListObjects"
            ],
            "Resource": "*"
        }
    ]
}
```
...be sure to update the resource ARNs (`staging.eem.example.com` and `eem.example.com`) in the policy to match your bucket names.
