> Web client dashboard for the EEM ETL repository [EEM ETL Repo](https://bitbucket.org/terndatateam/eemetlqueries/src/master/)

# Essential Environmental Measures (EEMs)

## Project description
The Essential Environmental Measures (EEMs) is an Australian Government Department of Environment and Energy (DoEE) lead program to identify measures that are necessary to track changes in the state of the environment and to improve access and use of measures data. The working group for development of EEMs for native vegetation under the DoEE have identified vegetation cover, ground cover, vegetation height and biomass as candidate EEMs for native vegetation. The project would perform data requirement analysis of the measures and, create and publish data products of EEMs for native vegetation.

EEMs could add to the effective monitoring of the ecosystem and inform scientists and policymakers on state and changes in the environment.  They can contribute to several policy initiatives such as the State of Environment reporting for native vegetation and national environmental and carbon accounting.
 ## About this repo
 This repository contains code for the Web Dashboard application which connects to the backend code described in the repository [https://bitbucket.org/terndatateam/eemetlqueries/src/master/](https://bitbucket.org/terndatateam/eemetlqueries/src/master/), i.e. the EEMETLQueries repository. Please navigate to the EEMETLQueries repository to get a more complete understanding of how the whole application works (```mandatory```)

## Application Details
![EEM Dashboard](images_readme/EEM_Dashboard_Screen.png "EEM Dasboard")

The above image shows a running EEM dashboard application. The application is built using [Angular](https://angular.io/). The records from the database are displayed using [AG-Grid](https://www.ag-grid.com/angular-getting-started/) component (The Ag-grid component allows easiy filtering and sorting of records among other things). The ag-grid is configured to allow pagination of records from the database for effeciency and speed. The records are retrieved from the backend database(```Postgresql```) through an API provided by the [PostgREST](http://postgrest.org/en/v6.0/index.html) ```api```. Some useful API endpoints are also shown in the ```EEM API``` link. The application also has a link to download all the csv data from the 4 datasets which were ingested (see repo [https://bitbucket.org/terndatateam/eemetlqueries/src/master/](https://bitbucket.org/terndatateam/eemetlqueries/src/master/) for the etl ingestion process). Also, when browsing for the data you can download the first 1000 records displayed in the grid as a csv file.

## Building a docker container for the EEM Dashboard
Because of how the whole EEM Application is engineered, all components of the application are build using ```docker containers```. Specifically the EEM Dashboard application a docker image needs to be build and pushed to [Docker Hub](https://hub.docker.com/). Currently the build script (mentioned below) will build the ```staging branch``` and push the resultant docker image to ```mosheh/eem-frontend``` public repository in Docker Hub. However this can be changed by editing the script mentioned below.(```Plan: We will create a common docker hub account for terndata developers to push docker images to a common repo```) 

To build a docker image:

1. Clone this repository
1. Install the dependencies: ```yarn``` (Assumption: Yarn is installed. To install ```yarn``` on Mac use ```brew``` on ubuntu: ```$curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - $echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list $sudo apt update $sudo apt install yarn```)

1. Run the script ```./eem_frontend_docker_image_build.sh``` 

What will the script do?

1. It will ask you for a version number you want to use (```e.g. v3.0.9```)
1. It will build the application (Angular)
1. Build a docker image (copies build files from /dist to the image, the image is an nginx docker base image)
1. Push the docker image to the Docker hub repo

Note: The EEM dashboard requires an API to talk to. Building a docker image using the steps outlined above assumes you are familiar with the instructions described in the repository [https://bitbucket.org/terndatateam/eemetlqueries/src/master/](https://bitbucket.org/terndatateam/eemetlqueries/src/master/), which is a ```mandatory``` read.

## Local development (Deploy)
Assuming you want to deploy the application locally and assuming you have an API running locally, see [PostgREST](http://postgrest.org/en/v6.0/) and a local database with eem data, simply run ```yarn run start:stagingLocal```. The application will be accessable from the url: ```http://localhost:4200```. Note that ```yarn run start:stagingLocal``` launches the application using the settings which point to the API at ```http:localhost:3000``` which is the default endpoint of the ```PostgREST``` API. Hence make sure that you have deployed locally the ```PostgREST``` api to listen on  port 3000.(Hint: To easily deploy the PostgREST API for testing follow the instructions from [http://postgrest.org/en/v6.0/install.html#docker](http://postgrest.org/en/v6.0/install.html#docker))
## Contributors

1. T. Saleeba
1. A. Sebastian
1. S. Guru
1. M. EliYahu

### Resources and technologies used
1. [Docker](https://www.docker.com/) (Technology used to deploy the application)
1. [Angular](https://angular.io/) (Technology used to build application)
1. [Ag-Grid](https://www.ag-grid.com/angular-getting-started/) (Technology use to display database record in a grid format)
1. [Bash Script](https://www.gnu.org/software/bash/)(Used to script the build process)

Big thanks to https://github.com/akveo/ngx-admin for the great starter template. Based on commit [66dff308e61edc56bd37c43d9f1ea447043c2e8d](https://github.com/akveo/ngx-admin/commit/66dff308e61edc56bd37c43d9f1ea447043c2e8d) from 27 Feb 2018.

---
