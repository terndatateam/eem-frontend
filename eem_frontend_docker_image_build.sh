# This script build the EEM dashboard application (Angular)
# It also creates a docker image (2 images, a versioned and lastest) and pushes it to hub.docker.com to Mosheh's public repo

if [ $# -ne 1 ] ; then
   echo usage: `basename $0` "<app_version>"
   echo e.g.: `basename $0` "v3.0.1"
   echo "For the latest version please see mosheh's docker hub repo:"
   echo "Hint: Search hub.docker.com for the image: eem-frontend"
   exit 1
fi

export APP_VERSION_NO=$1

# Build the app (this will generate a ./dist folder where all the compiled files will be)
yarn run build:staging-docker
# Copy csv download files to the dist folder
cp ./src/app/csv_downloads/* ./dist 

# Build the nginx docker container
# ternaustralia/eem-frontend
echo "Build the docker image now ....."
docker build -t ternaustralia/eem-frontend:${APP_VERSION_NO} -t ternaustralia/eem-frontend:latest .

# Push the docker image to docker hub
echo "Push the docker image to docker hub...."
docker login && docker push ternaustralia/eem-frontend:${APP_VERSION_NO} && docker push ternaustralia/eem-frontend:latest
echo "Completed"